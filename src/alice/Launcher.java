package alice;


public class Launcher {

	public static void main(String[] args) {

		ExampleDatabase exDb = null;

		if ( args.length != 0 && args[0] != null )
			exDb = new ExampleDatabase(args[0]);
		else
			Debug.reportErrorAndQuit("Launcher", "Please supply A.L.I.C.E with an input file");

		// Creates a neural network with a flavor given by the types possible,
		// the number of features, and an initial weight
		NeuralNet aliceNet = new NeuralNet(exDb.getNumberOfFeatures(),
				NeuralNet.NetworkType.Perceptron, .1f);

		// Trains the neural net with the example database and a learning rate
		aliceNet.train(exDb, .5);
	}
}
