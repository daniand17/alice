package alice;

public class Neuron {

	private double inputvalue = 0f;
	private double[] arcWeights = null;
	private Neuron[] parentNodes = null;

	private int count = 0;
	private int numNodes = 0;

	public Neuron(int numConnectedNodes) {
		numNodes = numConnectedNodes;
		arcWeights = new double[numNodes];
		parentNodes = new Neuron[numNodes];
	}

	public void setInputValue(double val) {
		inputvalue = val;
	}

	public void addParent(Neuron node, float weight) {
		parentNodes[count] = node;
		arcWeights[count] = weight;
		count++;
	}

	public double getSigmoidOutput() {

		double sum = 0;
		// If this is an output or interior node
		if ( count != 0 )
			for (int i = 0; i < arcWeights.length; i++)
				sum += parentNodes[i].getSigmoidOutput() * arcWeights[i];
		else
			// If this is an input node
			sum = inputvalue;
		return sigmoid(sum);
	}

	public double getThresholdOutput() {
		double sum = 0;
		// If this is an output or interior node
		if ( count != 0 )
			for (int i = 0; i < arcWeights.length; i++)
				sum += parentNodes[i].getSigmoidOutput() * arcWeights[i];
		else
			// If this is an input node
			sum = inputvalue;
		return sum;
	}

	private double sigmoid(double val) {
		return 1 / (1 + Math.pow(Math.E, -val));
	}

	public void updateWeights(double alpha, double trueVal) {

		// TODO: This isn't right according to back propagation

		// Get the output value for this node
		double ai = this.getSigmoidOutput();
		// Get the error given the output value and the passed in true value
		double dj = ai * (1 - ai) * (trueVal - ai);

		for (int i = 0; i < arcWeights.length; i++)
			arcWeights[i] = arcWeights[i] + dj * parentNodes[i].getSigmoidOutput();

		for (Neuron pn : parentNodes)
			pn.updateWeights(alpha, trueVal);
	}
}
