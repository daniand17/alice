package alice;

import java.util.ArrayList;
import java.util.List;

public class ExampleDatabase {

	// Holds the list of examples
	private List<Example> exampleList = null;
	private List<String> outputCategories = null;
	private List<Feature> features = null;

	private int numFeatures = 0;

	public ExampleDatabase(String fileName) {

		FileScanner fileio = new FileScanner(fileName);
		// Create the list of examples
		exampleList = new ArrayList<Example>();
		// Create the list of possible output categories
		outputCategories = new ArrayList<String>();
		// Create the list of possible features
		features = new ArrayList<Feature>();

		// Parse the info that describes the data
		parseDataInformation(fileio);
		// parseDatasetParams(fileio);
		Debug.log("ExampleDatabase", "Data parsed");

		// Parse the input file and populate the list
		parseAndPopulateExampleList(fileio);
		Debug.log("ExampleDatabase", "Database populated");
	}

	private void parseDataInformation(FileScanner io) {
		// First get the number of features
		numFeatures = Integer.parseInt(io.parseNextLine()[0]);

		String[] currLine = null;

		for (int i = 0; i < numFeatures; i++) {

			if (io.hasNextLine()) {
				// Get the feature info
				currLine = io.parseNextLine();

				Feature newFeat = new Feature(currLine[0]);

				for (int j = 1; j < currLine.length; j++)
					newFeat.addPossibleValue(currLine[j]);
				features.add(newFeat);
			}
		}

		Debug.log("ExampleDatabase", "Added " + features.size() + " features.");

		// Figure out how many possible output labels we have
		int numOutputs = Integer.parseInt(io.parseNextLine()[0]);
		// Get the possible output labels
		for (int i = 0; i < numOutputs; i++)
			outputCategories.add(io.parseNextLine()[0]);

		Debug.log("ExampleDatabase", "Added " + outputCategories.size()
				+ " possible output values.");
	}

	private void parseDatasetParams(FileScanner io) {
		int numOutputs = Integer.parseInt(io.parseNextLine()[0]);

		for (int i = 0; i < numOutputs; i++)
			outputCategories.add(io.parseNextLine()[0]);
	}

	public void summary() {

		String msg = "Summary";
		msg += "\nThere are " + features.size() + " features:\n";

		for (Feature feat : features) {
			msg += "    " + feat.getName() + ": ";

			for (String val : feat.getPossibleValues())
				msg += " " + val;
			msg += "\n";
		}
		msg += "The possible outputs are:\n    ";
		for (String output : outputCategories)
			msg += output + ", ";

		Debug.log("ExampleDatabase\n", msg);
	}

	private void parseAndPopulateExampleList(FileScanner io) {

		String[] currLine = null;

		while (io.hasNextLine()) {
			currLine = io.parseNextLine();
			// Create the example and find the output category at first token
			Example newEx = new Example(currLine[1]);

			for (int i = 2; i < features.size() + 2; i++) {
				// Create the feature to add to the example
				ExampleFeature newExFeat = new ExampleFeature(features.get(
						i - 2).getName(), currLine[i]);
				// Add the new feature to the example

				newEx.addFeature(newExFeat);
			}
			// Add the new example with features to the example list
			exampleList.add(newEx);
		}
	}

	public List<Example> getCopyOfExamples() {

		List<Example> newList = new ArrayList<Example>();

		for (Example ex : exampleList)
			if (ex != null)
				newList.add(ex);

		return newList;
	}

	public int getNumberOfFeatures() {
		return numFeatures;
	}

	public List<String> getOutputValues() {
		return outputCategories;
	}
}
