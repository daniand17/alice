package alice;

public class Debug {

	private static final String format = "%-20s>%s%n";

	public static void reportErrorAndQuit(String tag, String msg) {

		if (tag != null && msg != null) {
			tag += ": ";
			System.err.printf(format, tag, msg);
		}
		System.exit(0);
	}

	public static void log(String tag, String msg) {

		if (tag != null && msg != null)
			System.out.printf(format, tag, msg);
	}
}
