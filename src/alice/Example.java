package alice;

import java.util.ArrayList;
import java.util.List;

public class Example {

	private List<ExampleFeature> features = null;
	private String category = null;

	public Example(String category) {
		features = new ArrayList<ExampleFeature>();
		this.category = category;
	}

	/**
	 * Adds a feature to this example
	 * 
	 * @param feature
	 *            the feature to add
	 */
	public void addFeature(ExampleFeature feature) {

		if (feature != null && features != null)
			features.add(feature);
		else
			Debug.reportErrorAndQuit("Example",
					"Attempted to add a null feature to the features list");
	}

	/**
	 * Gets the category or output label of this example.
	 * 
	 * @return the category/output label
	 */
	public String getCategory() {
		return category;
	}

	public List<ExampleFeature> getFeatureValues() {
		return features;
	}

	public boolean hasFeatureValue(String featureName, String featureValue) {

		boolean hasValue = false;
		for (ExampleFeature currFeat : features)
			// If this example has both the name of the feature and the correct
			// value for the feature, then it has the value
			if (currFeat.getName().equalsIgnoreCase(featureName)
					&& currFeat.getValue().equalsIgnoreCase(featureValue))
				hasValue = true;

		return hasValue;
	}
}
