package alice;

public class ExampleFeature extends Feature {

	private String value = null;

	public ExampleFeature(String name, String value) {
		super(name);
		this.value = value;
	}

	public String getValue() {

		if (value == null)
			Debug.reportErrorAndQuit("ExampleFeature",
					"The value for a feature was null");
		return value;
	}
}
