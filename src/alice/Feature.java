package alice;

import java.util.ArrayList;
import java.util.List;

public class Feature {

	private String name = null;
	private List<String> possibleValues = null;

	public Feature(String name) {
		this.name = name;
		possibleValues = new ArrayList<String>();
	}

	public String getName() {

		String theName = name;

		if (name == null)
			Debug.reportErrorAndQuit("Feature",
					"The name of a feature was null");

		return theName;
	}

	public List<String> getPossibleValues() {
		if (possibleValues.size() == 0)
			Debug.reportErrorAndQuit("Feature",
					"A feature containing no values was accessed");
		return possibleValues;
	}

	public void addPossibleValue(String newValue) {

		if (newValue != null)
			possibleValues.add(newValue);
		else
			Debug.reportErrorAndQuit("Feature",
					"Tried to add a null string to a feature");
	}

}
