package alice;

import java.util.List;

public class Utilities {
	/**
	 * Helper method to determine the binary logarithm of x, converting from the
	 * natural log of x
	 * 
	 * @param x
	 *            the variable to take the binary logarithm of
	 * @return the binary log2(x)
	 */
	public static float log2(float x) {
		// Determine whether x is really close to 1 or 0, and should be either
		// of those
		float diff1 = Math.abs(1.0f - x);
		float diff0 = Math.abs(0.0f - x);
		float tol = 0.00001f;

		if ( diff1 < tol )
			return 0.0f;
		else if ( diff0 < tol )
			return 0.0f;

		return (float) (Math.log(x) / Math.log(2));
	}

	/**
	 * Calculates the entropy of a Boolean random variable.
	 * 
	 * @param q
	 *            the probability of the boolean random variable
	 * @return the entropy of q
	 */
	public static float entropy(float q) {
		// TODO make sure to calculate entropy for a non-boolean variable
		return -q * log2(q) - (1 - q) * log2(1 - q);
	}

	/**
	 * Calculates the information gain of a given feature in a list of examples.
	 * 
	 * @param feat
	 *            the BinaryFeature object to query a value of
	 * @param examples
	 *            the list of examples to check
	 * @return the float value of information gained by the feature with this
	 *         list of examples
	 */
	public static float importance(Feature feat, List<Example> examples) {
		int numPos = 0;
		int numNeg = 0;

		// Iterate through the examples, and count the number examples with a
		// given label
		for (Example curr : examples)
			/*
			 * TODO if (curr.getCategory().equalsIgnoreCase(
			 * trainSet.getOutputLabel().getFirstValue()))
			 */
			numPos++;
		// else
		numNeg++;

		// Calculation of entropy and the remainder
		float first = entropy((float) numPos / (numNeg + numPos));
		float second = remainder(feat, examples);
		// Returns the importance, or information gain for this feature
		return first - second;
	}

	/**
	 * Gets the remainder of a binary feature, or in other words, the amount of
	 * information left if one was to split a node on this feature.
	 * 
	 * @param feat
	 *            the binary feature to check
	 * @param examples
	 *            the list of examples
	 * @return the float representation of the information left by this example
	 */
	public static float remainder(Feature feat, List<Example> examples) {
		float rem = 0f;
		// Iterate through each value of the binary feature and find its
		// contribution to the
		// remainder
		for (String value : feat.getPossibleValues()) {
			float p = 0f;
			float n = 0f;

			for (Example currEx : examples) {
				if ( currEx.hasFeatureValue(feat.getName(), value) )
					/*
					 * TODO if (trainSet.getOutputLabel().getFirstValue()
					 * .equals(currEx.getCategory()))
					 */
					p += 1;
				else
					n += 1;
			}
			float first = (p + n) / (float) examples.size();
			float second = 0;
			// Make sure we check the zero case to prevent NaN issues
			if ( p + n > 0 )
				second = entropy(p / (p + n));
			// Increment the remainder contribution for this BinaryFeature value
			rem += (first * second);
		}
		return rem;
	}

	public static double roundToTenths(double value) {

		return ((double) ((int) (value * 100))) / 100;

	}
	
	public static double roundToThousandths(double value) {

		return ((double) ((int) (value * 1000))) / 1000;

	}
}
