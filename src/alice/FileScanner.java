package alice;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileScanner {

	private Scanner in = null;
	private File inFile = null;
	private String regex = "\\s+";

	public FileScanner(String fileName) {
		inFile = new File(fileName);

		// In this case, initializes the scanner using the public method
		rewind();

		if (!inFile.canRead())
			Debug.reportErrorAndQuit("FileScanner",
					"Input file found but cannot be read. Quitting.");
		else
			Debug.log("FileScanner",
					"Input file can be read. Connection established.");

	}

	public String[] parseNextLine() {
		String line = getNextSignificantLine();

		if (line == null)
			Debug.reportErrorAndQuit("FileScanner",
					"Attempted to access past end of file. End of file reached.");

		return line.split(regex);
	}

	private String getNextSignificantLine() {

		String line = null;
		while (in.hasNextLine()) {
			line = in.nextLine().trim();
			if (isLineSignificant(line))
				return line;
		}

		return line;
	}

	private boolean isLineSignificant(String line) {

		if (line == null || line.length() == 0 || line.startsWith("//"))
			return false;
		else
			return true;
	}

	public void rewind() {

		// Close the scanner before reopening it.
		if (in != null)
			in.close();

		try {
			in = new Scanner(inFile);
		} catch (FileNotFoundException e) {
			Debug.reportErrorAndQuit("FileScanner",
					"Error opening file input. Quitting.");
			System.exit(0);
		}

		in = in.useDelimiter(regex);
	}

	public boolean hasNextLine() {
		return in.hasNext();
	}

	public int getLineCount() {

		int count = 0;
		rewind();
		while (this.hasNextLine()) {
			this.parseNextLine();
			count++;
		}

		rewind();
		return count;

	}
}
