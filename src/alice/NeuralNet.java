package alice;

import java.util.List;

public class NeuralNet {

	public enum NetworkType {
		Perceptron;
	}

	private Neuron outputNode = null;
	private Neuron[] inputNodes = null;

	/**
	 * Creates a neural net with various options for which type of network to
	 * create
	 * 
	 * @param numInputs
	 */
	public NeuralNet(int numInputs, NetworkType type, float defaultWeight) {

		// sets the root of the neural net to be the output node
		outputNode = new Neuron(numInputs + 1);
		// Creates an array of input nodes for easy referencing
		inputNodes = new Neuron[numInputs + 1];
		// Set the inputs for the requested values
		for (int i = 0; i < numInputs; i++)
			inputNodes[i] = new Neuron(0);

		switch (type) {
		case Perceptron:
			this.buildPerceptron(defaultWeight, numInputs);
			break;
		}
	}

	public double getRawOutputFromInputVector(double[] inputVector) {

		for (int i = 0; i < inputVector.length; i++)
			inputNodes[i].setInputValue(inputVector[i]);

		return outputNode.getSigmoidOutput();
	}

	private void buildPerceptron(float startWeight, int numInputs) {

		// Set the -1 input value for one of the input nodes
		inputNodes[numInputs] = new Neuron(0);
		inputNodes[numInputs].setInputValue(-1f);

		for (Neuron curr : inputNodes)
			outputNode.addParent(curr, startWeight);

		Debug.log("NeuralNet", "\n\tPerceptron created: " + "\n\t# Input nodes: "
				+ inputNodes.length + "\n\tInitial Weight: " + startWeight);
	}

	public void train(ExampleDatabase exDb, double alpha) {

		// The input vector of floats
		double[] inputVector = new double[exDb.getNumberOfFeatures()];

		// Number of examples in the training set
		int numExamples = exDb.getCopyOfExamples().size();
		// How many examples the neural net got right
		double numRight = 0;

		for (Example currEx : exDb.getCopyOfExamples()) {
			// Figure out the category for this example and set it to 1 or 0. In
			// this case a 1 is stayIn and a 0 is stayOut
			double category = currEx.getCategory().equals(exDb.getOutputValues().get(0)) ? 0.0
					: 1.0;

			List<ExampleFeature> exFeats = currEx.getFeatureValues();
			for (int i = 0; i < exFeats.size() - 1; i++)
				inputVector[i] = Double.parseDouble(exFeats.get(i).getValue());

			// Convert bear/bull to 0 or 1
			inputVector[inputVector.length - 1] = exFeats.get(exFeats.size() - 1).getValue()
					.equals("bear") ? 0 : 1;

			// Get the output of the neural net for this example
			double hw = this.getRawOutputFromInputVector(inputVector);

			// Adjust output according to threshold
			hw = hw >= 0 ? 1.0 : 0.0;

			double delta = (int) (category - hw);

			if ( delta != 0 ) {
				// Adjust weights if prediction was wrong
				outputNode.updateWeights(alpha, category);

			} else
				numRight++;
		}

		Debug.log("NeuralNet", "\n\tFinished training:\n\t\tExample count: " + numExamples
				+ "\n\t\tNumber correct: " + (int) numRight + "\n\t\tPrediction Accuracy: "
				+ Utilities.roundToThousandths((numRight / numExamples) * 100) + "%");
	}
}
